drop table if exists import_default;
create table import_default(
    id int primary key auto_increment,
    overlay_graphic_file varchar(64),
    overlay_visible int,
    max_periods int default 3,
    dir varchar(64),
    sport char(32),
    visibility enum('All', 'Coach', 'Hidden') default 'All',
    inmediate int default 0,
    event_visible int default 1,
    vdir char(64),
    last_period_duration int,
    manual_schedule int,
    scope enum('private', 'public') default 'private'
)engine=innodb charset=utf8;

insert into import_default(overlay_visible, dir, sport) values (1, 'video', 'Hockey');
