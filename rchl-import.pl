#!/usr/bin/env perl
use strict;
use warnings;
use 5.010;
use Dotenv qw//;
use Data::Dumper qw/Dumper/;
use FindBin qw/$Bin/;
use lib "$Bin/lib";
use GOS::Config qw/$cfg load_config/;
use Log::Log4perl qw/:easy/;
use Getopt::Lucid qw/:all/;
use GOS::DB qw//;
use GOS::Spreadsheet qw//;
use DateTime;
use DateTime::Format::Strptime;
use Try::Tiny qw/try catch/;
use List::Util qw/ any all /;

usage() unless @ARGV;

load_config($Bin . '/conf/config.pl');

unlink $cfg->{l4p}->{filename};

Log::Log4perl->init("$Bin/l4p.conf");
Dotenv->load("$Bin/../config/.env");

my @spec = (
    Param('file|f'),
    Param('duration|d'),
    Param('league|l'),
    Switch('create_teams'),
    Param('date_format'),
);

my $opt = Getopt::Lucid->getopt(\@spec)->validate({requires => [qw/file duration/]});
my $date_format = $opt->get_date_format;
my $league = $opt->get_league || 'rchl';
my $dbh = GOS::DB->connect($league);

my ($tz) = $dbh->selectrow_array('select time_zone from league');
DEBUG "Using time zone: $tz";

{
    my $now = DateTime->now;
    $now->set_time_zone($tz);
    my $dbtz = $now->strftime('%z');
    $dbtz =~ s/^(.+)(\d{2})$/$1:$2/;

    $dbh->do(sprintf(qq{set time_zone = '%s'}, $dbtz));
}

my $parser = DateTime::Format::Strptime->new(
    pattern => $date_format,
    time_zone => $tz,
) or die 'failed to create parser';

my $division_id_map = $dbh->selectall_hashref('select id, division from division', 'division');
my $defaults = $dbh->selectrow_hashref('select * from import_default limit 1');

my $today = DateTime->now(time_zone => $tz);
my ($over_adjusted, $inserted, $skipped, $failed, $adjusted) = (0, 0, 0, 0, 0);
# start, end(optional), team1, team2, division, arena
my $data = GOS::Spreadsheet::read_file($opt->get_file);

#system("mysqldump -udhaval -K gos_" . $league . " > " . $league . '-'.time().'.sql');

$dbh->begin_work;

clear_events();
validate_duration($data);
create_teams($data) if ($opt->get_create_teams);
create_events($data);
$dbh->commit;

my $summary = "\n" . <<SUMMARY;
Total inserted: $inserted
Total skipped: $skipped
Total failed: $failed
Total adjusted: $adjusted
Total over adjusted 30 minutes: $over_adjusted
SUMMARY

INFO $summary;

sub clear_events {
    $dbh->do('DELETE FROM event WHERE start > ' . $today->epoch);
}

sub validate_duration {
    my $data = shift;

    foreach my $r (@$data) {
        try {
            my $start = parse_date($r->{start}) or
                die 'Invalid date ' . $r->{start};

            # parse end date if column exists in csv
            my $end = $r->{end} && parse_date($r->{end}) or
                    die 'Invalid date ' . $r->{end};

            $r->{start_ori} = $r->{start};
            $r->{end_ori} = $r->{end} if $r->{end};
            $r->{start} = $start;
            $r->{end} = $end if $end;
        }
        catch {
            say Dumper $r;
            say $_;
            exit;
        };
    }

    my @sorted = sort {
        $a->{arena} cmp $b->{arena} or DateTime->compare($a->{start}, $b->{start});
    } @$data;

    my $prev;
    my $prev_end;
    my $prev_loc = '';
    my @future_events;
    my $location_id_map = $dbh->selectall_hashref('select id, location from location', 'location');

    foreach my $r (@sorted) {
        my $start = $r->{start};

        if (!$start) {
            INFO 'Invalid date value ' . $r->{start};
            ++$skipped;
            next;
        }

        # Ignore past events
        if ($start < $today) {
            #DEBUG 'Skipped old dated: ' . $r->{date};
            ++$skipped;
            next;
        }

        my $loc = $location_id_map->{$r->{arena}};

        if (!$loc) {
            INFO 'location ' . $r->{arena} . ' not found, ' . $r->{start};
            ++$skipped;
            next;
        }

        if ($r->{arena} ne $prev_loc) {
            $prev = undef;
            $prev_loc = $r->{arena};
        }

        if ($prev && $start->epoch - $prev->{end}->epoch < 119) {
            # if overlaps, throw error
            my $msg;
            $msg = "$r->{team1} vs $r->{team2} at $r->{arena}, start: ". $r->{start} . " overlap with previous event.\n";

            $msg .= "$prev->{team1} vs $prev->{team2} at $prev->{arena},  " . $prev->{start}->strftime('%D %T') . ' to ' . $prev->{end}->strftime('%D %T') ;
            #LOGDIE $msg;
            ERROR $msg;
            $prev = $r;
            next;
        }

        # if csv has no end time then calculate using duration parameter
        unless ($r->{end}) {
            $r->{end} = $start->clone;

            my $minutes = $opt->get_duration;

            $r->{end}->add(minutes => $minutes);
        }
        $prev = $r;

        push @future_events, $r;
    }
    DEBUG scalar @future_events . ' events to insert';
    @$data = @future_events;
}

sub create_events {
    my $data = shift;
    my $team_id_map;
    my $rs = $dbh->selectall_arrayref('select t.id, t.name, d.division from team t
        inner join division d on d.id = t.division_id');

    foreach my $r (@$rs) {
        $team_id_map->{$r->[2]}->{$r->[1]} = $r->[0];
    }

    my $packages = $dbh->selectcol_arrayref(<<SQL);
    SELECT id FROM subscription_plan WHERE default_package = 1
SQL

    my $tmpl = $dbh->selectall_hashref(<<SQL, 'tmpl_name');
    SELECT id, tmpl_name, location_id, sport, cam_record, cam_live, primary_live
    FROM view_template
SQL

    foreach my $t (keys %$tmpl) {
        my %cam;
        $cam{$_}->{record} = 1 foreach (split ',', delete $tmpl->{$t}->{cam_record});
        $cam{$_}->{broadcast} = 1 foreach (split ',', delete $tmpl->{$t}->{cam_live});
        $cam{$tmpl->{$t}->{primary_live}}->{primary_live} = 1
            if $tmpl->{$t}->{primary_live};

        $tmpl->{$t}->{camera} = \%cam;
    }

    foreach my $row( @$data ) {
        my $view_tmpl = $tmpl->{$row->{arena}};

        unless ($view_tmpl) {
           INFO 'skipped missing view tmpl name ' . $row->{arena} . ' at ' . $row->{start};
           ++$skipped;
           next;
        }

        my $age_group = $row->{age_group};
        my $category = $row->{category};
        my $division = $row->{division} || $cfg->{age_group}->{$age_group} . ' ' . $category;
        my $start = delete $row->{start};
        my $end = delete $row->{end};

        my $team_id1 = $team_id_map->{$division}->{$row->{team1}} ||
            $team_id_map->{$division}->{UNK};

        unless ($team_id1) {
            ERROR "team not found $row->{team1} under $division at $row->{start}";
            ++$skipped;
            next;
        }

        my $team_id2 = $team_id_map->{$division}->{$row->{team2}} ||
            $team_id_map->{$division}->{UNK};

        unless ($team_id2) {
            ERROR "team not found $row->{team2} under $division at $row->{start}";
            ++$skipped;
            next;
        }

        my %new = (
            %$defaults,
            start => $start->epoch,
            end => $end->epoch,
            broadcast => 1,
            rebroadcast => 1,
            uplaod => 1,
            team_id1 => $team_id1,
            team_id2 => $team_id2,
            location_id => $view_tmpl->{location_id},
            division_id => $division_id_map->{$division}->{id},
            playoff => 1,
            source => 'add',
            local_record => 1,
            raw => join ", ", values %$row
        );

        if (_create_event(\%new, $packages, $view_tmpl)) {
            ++$inserted;
        }
        else {
            say $row->{start_ori} . ' to ' . $row->{end_ori} . ' ###  ' . $start->epoch . ' to ' . $end->epoch;
            ++$failed;
        }
    }
}

sub _create_event {
    my ($row, $packages, $view_tmpl) = (@_);
    state @fields;
    @fields = event_fields() unless @fields;
    state $sth_insert_event = sth_insert_event(@fields);
    state $local_vod = local_vod();

    my $dt = DateTime->from_epoch(epoch => $row->{start});
    $dt->set_time_zone($tz);

    my $start_dmy = $dt->dmy;
    state $dmy = $start_dmy;

    if ($dmy ne $start_dmy) {
        $local_vod = local_vod();
        $dmy = $start_dmy;
    }
    my $lv;

    if ($local_vod) {
        $lv = $local_vod->{$row->{location_id}}->{$dt->strftime('%H:%M:%S')};

        unless ($lv) {
            # don't skip as per john said on 5/12/18
            # INFO 'skipped event, short of local vod for the day';
            # ++$skipped;
            # return;
        }
    }

    my @data;

    foreach my $f (@fields) {
        my $val = exists $row->{$f} ? $row->{$f} : undef;
        $val = undef if defined $val && $val eq '';

        push @data, $val;
    }

    my $id = try {
        my @d = @data;
        #say join ", ", map { $_ //= '' } @d;
        #say $row->{raw};
        $sth_insert_event->execute(@data);

        my $id =  $dbh->last_insert_id(undef, undef, 'evnet', 'id')
            or die "failed to insert event";

        #- Assign Package
        foreach my $pid (@$packages) {
            $dbh->do(<<SQL, undef, $pid, $id);
            INSERT INTO event_package(id_package, id_event) values(?,?)
SQL
        }

        #- Assign Camera
        while (my ($camera, $setting) = each %{ $view_tmpl->{camera} }) {
            my $setting = $view_tmpl->{camera}->{$camera};
            my @bind = ($id, $camera, $setting->{broadcast}, $setting->{primary_live},
                $setting->{record}, 'local');

            $dbh->do(<<SQL, undef, @bind);
            INSERT INTO event_stream
            (event_id, camera, broadcast, primary_live, record, record_at) values
            (?,?,?,?,?,?)
SQL
        }

        if ($lv) {
            $dbh->do(<<SQL, undef, $id, $lv);
            INSERT INTO event_local_vod(event_id, local_vod_name) values(?,?)
SQL
        }
        return $id;
    } catch {
        ERROR $_;
        ERROR Dumper \@data;
        undef;
    };
    return $id;
}

sub parse_date {
    my ($str) = @_;

    my $date = $parser->parse_datetime($str);
    return $date;
}

sub event_fields {
    my @fields = (qw/start end broadcast rebroadcast upload team_id1 team_id2 location_id
        division_id overlay_visible playoff dir source local_record inmediate max_periods sport visibility
        event_visible vdir last_period_duration manual_schedule scope/);

    my $sth = $dbh->column_info(undef, undef, 'event', '%');
    my @col;

    while (my $r = $sth->fetchrow_hashref) {
        push @col, $r->{COLUMN_NAME} if any { $_ eq $r->{COLUMN_NAME} } @fields;
    }

    return @col;
}

sub local_vod {
    return {} unless $league eq 'rchl';
    $dbh->tables(undef, undef, 'local_vod') or return;

    my %vod;

    for my $row (@{$dbh->selectall_arrayref('SELECT location_id, name, time FROM local_vod order by location_id, cast(name as unsigned)')}) {
        next unless $row->[2];
        $vod{$row->[0]} //= {};

        $vod{$row->[0]}->{$row->[2]} = $row->[1];
    }
    return \%vod;
}

# create missing divisions and teams under it
sub create_teams {
    my ($data) = @_;

    my %map;
    my $rs = $dbh->selectall_arrayref('SELECT t.id, t.name, d.division, t.division_id FROM team t join
        division d on t.division_id = d.id', { Slice => {} });

    for my $row (@$rs) {
        $map{$row->{division}}->{$row->{name}} = $row->{id};
    }

    for my $row (@$data) {
        unless ($division_id_map->{$row->{division}}) {
            $dbh->do('insert into division(division, active, visible) values(?,?,?)', undef,
                $row->{division}, 1, 1);

            $division_id_map->{$row->{division}} = {
                id => $dbh->last_insert_id(undef, undef, 'division', 'id'),
                division => $row->{division},
            };
        }

        unless ($map{$row->{division}}->{$row->{team1}}) {
            $dbh->do('insert into team(division_id, name, active, league) values(?,?,?,?)', undef,
                $division_id_map->{$row->{division}}->{id}, $row->{team1}, 1, $league);

	    $map{$row->{division}}->{$row->{team1}} = $dbh->last_insert_id(undef, undef, 'team', 'id');

        }

        unless ($map{$row->{division}}->{$row->{team2}}) {
            $dbh->do('insert into team(division_id, name, active, league) values(?,?,?,?)', undef,
                $division_id_map->{$row->{division}}->{id}, $row->{team2}, 1, $league);

	    $map{$row->{division}}->{$row->{team2}} = $dbh->last_insert_id(undef, undef, 'team', 'id');
        }
    }
}

sub sth_insert_event {
    my @fields = @_;
    my $f = join ', ', @fields;
    my $p = join ', ', map { '?' } @fields;

    my $sth = $dbh->prepare(sprintf(<<SQL, $f, $p));
    INSERT INTO event(%s) values(%s)
SQL

    return $sth;
}

sub log_level {
    return $cfg->{l4p}->{level};
}

sub log_filename {
    return $cfg->{l4p}->{filename};
}

sub usage {
    my $f = __FILE__;
    say <<USAGE;
    $f -f <filepath> -d <event duration in minutes>
USAGE

}
