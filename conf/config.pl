use FindBin;

return {
    db_user => 'wideas',
    db_password => 'GY%$TRHef%43',
    l4p => {
        level => 'INFO',
        filename => "$FindBin::Bin/log/app.log",
    },
    age_group => {
        ATM => 'Atom',
        BAN => 'Bantam',
        MAT => 'Minor Atom',
        MBN => 'Minor Bantam',
        MGJ => 'Midget Junior',
        MGT => 'Midget',
        MMD => 'Minor Midget',
        PWE => 'Peewee',
        MPW => 'Minor Peewee',
        JUV => 'U21',
    },
    date_format => '%Y-%m-%d %H:%M',
    duration => {
        'PWE' => 75,
        'MBN' => 75,
        'BAN' => 90,
        'MGT' => 90,
        'JUV' => 120,
    },
    'gthl' => {
        time_zone => 'America/New_York',
    },
    'rchl' => {
        time_zone => 'America/Edmonton'
    },
    'teamsite' => {
        time_zone => 'America/Denver'
    }
};
