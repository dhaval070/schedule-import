#!/usr/bin/env perl
use strict;
use warnings;
use 5.010;
use Data::Dumper qw/Dumper/;
use FindBin qw/$Bin/;
use Getopt::Lucid qw/:all/;

usage() unless @ARGV;

my @spec = (
    Param('file|f'),
    Param('duration|d'),
    Param('league|l'),
    Param('date_format'),
);

my $opt = Getopt::Lucid->getopt(\@spec)->validate({requires => [qw/file league duration date_format/]});
my $l = $opt->get_league;
my $bin = "$Bin/${l}-import.pl";

my @opt = ("-d", $opt->get_duration, "-f", $opt->get_file, "--date_format=". $opt->get_date_format);

unless (-e $bin) {
    push @opt, '-l', $l, "--create_teams";
    $bin = "$Bin/import.pl";
}

system($bin, @opt);
