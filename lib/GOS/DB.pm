package GOS::DB;
use strict;
use warnings;
use DBI;

sub connect {
    my ($self, $league) = @_;

    my $dbname = 'gos_' . $league;
    my $host = $ENV{DB_HOST};
    my $user = $ENV{'DB_USER'};
    my $pass = $ENV{'DB_PASS'};

    DBI->connect("dbi:mysql:host=$host;dbname=$dbname", $user, $pass, { RaiseError => 1 });
}

1;
