package GOS::Spreadsheet;
use strict;
use warnings;
use Spreadsheet::Read;
use Data::Dumper;
use 5.010;

sub read_file {
    my ($filename, $fields) = @_;

    die "file not found - $filename" unless -r $filename;

    my $book = Spreadsheet::Read->new($filename);
    my $sheet = $book->sheet(1);

    my @data;
    my $maxrow = $sheet->maxrow;
    my $maxcol = $sheet->maxcol;
    my $start = 1;

    my @head;

    if ($fields) {
        @head = @$fields;
    } else {
        @head = $sheet->row(1);
        $start = 2;
    }

    for (my $i = $start; $i <= $maxrow; ++$i) {
        my @row = $sheet->row($i);

        trim($_) foreach (@row);

        eval {
            push @data, row2hash(\@row, \@head);
        };
        die $@ . " at row $i" if $@;
    }
    return \@data;
}

sub row2hash {
    my ($row, $fields) = @_;

    my %rowh;
    foreach my $index (0..(@$fields - 1)) {
        $rowh{$fields->[$index]} = $row->[$index];
    }
    return \%rowh; 
}

sub trim {
    $_[0] =~ s/^\s+|\s+$//g;
}
1;
