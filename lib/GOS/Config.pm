package GOS::Config;
use base Exporter;
use Data::Dumper;
use Config::JFDI;

our $cfg;
our @EXPORT_OK = qw/$cfg load_config/;

sub load_config {
    my ($path, $parent) = @_;
    my $config = Config::JFDI->new(name => 'local', path => $path)->get;

    my $return;

    if ($parent) {
        $return = $cfg->{$parent} = $config;
    }
    else {
        $return = $cfg = $config;
    }
    return $return;
}

1
