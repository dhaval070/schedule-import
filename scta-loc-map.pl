#!/usr/bin/env perl
use strict;
use warnings;
use 5.010;
use Data::Dumper qw/Dumper/;
use FindBin qw/$Bin/;
use lib "$Bin/lib";
use Log::Log4perl qw/:easy/;
use GOS::DB qw//;
use GOS::Config qw/$cfg load_config/;
use Text::CSV;

load_config($Bin . '/conf/config.pl');

Log::Log4perl->init("$Bin/l4p.conf");

my $league = $ARGV[0];# 'sctahockey';
my $dbh = GOS::DB->connect($league);

my $csv = Text::CSV->new();

open my $fh, '<', $ARGV[2] or die $!;

my $res = $dbh->selectall_hashref("select id, location from location", 'id');

my $map = {};

while (my $row = $csv->getline($fh)) {
    next unless $row->[1];
    unless (exists $res->{$row->[1]}) {
        warn "not found " . $row->[0];
    }

    $map->{$row->[0]} = $res->{$row->[1]}->{location};
}
close($fh);

open $fh, '<', $ARGV[1] or die $!;

my $row = $csv->getline($fh);
say join ",", @$row;

while (my $row = $csv->getline($fh)) {
    unless ($map->{$row->[4]} // 0) {
        warn "loc not found " . $row->[4];
        next;
    }

    $row->[4] = $map->{$row->[4]};
    say join ",", @$row;
}

sub log_level {
    return $cfg->{l4p}->{level};
}

sub log_filename {
    return $cfg->{l4p}->{filename};
}
