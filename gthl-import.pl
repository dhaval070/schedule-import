#!/usr/bin/env perl
use strict;
use warnings;
use 5.010;
use Dotenv qw//;
use Data::Dumper qw/Dumper/;
use FindBin qw/$Bin/;
use lib "$Bin/lib";
use GOS::Config qw/$cfg load_config/;
use Log::Log4perl qw/:easy/;
use Getopt::Lucid qw/:all/;
use GOS::DB qw//;
use GOS::Spreadsheet qw//;
use DateTime;
use DateTime::Format::Strptime;
use Try::Tiny qw/try catch/;
use List::Util qw/ any all /;

usage() unless @ARGV;

load_config($Bin . '/conf/config.pl');
load_config("$Bin/conf/team_codes.conf", 'team_code');

unlink $cfg->{l4p}->{filename};

Log::Log4perl->init("$Bin/l4p.conf");
Dotenv->load("$Bin/../config/.env");

my @spec = (
    Param('file|f'),
    Param('duration|d'),
    Param('date_format'),
);

my $league = 'gthl';
my $opt = Getopt::Lucid->getopt(\@spec)->validate({requires => [qw/file duration date_format/]});
my $dbh = GOS::DB->connect($league);
my @fields;

@fields = qw/schedule_code date age_group category shd_code team1 team2 home_score
    away_score reschedule schedule_completed arena playoff game_type/;

my ($tz) = $dbh->selectrow_array('select time_zone from league');
DEBUG "Using time zone: $tz";

{
    my $now = DateTime->now;
    $now->set_time_zone($tz);
    my $dbtz = $now->strftime('%z');
    $dbtz =~ s/^(.+)(\d{2})$/$1:$2/;

    $dbh->do(sprintf(qq{set time_zone = '%s'}, $dbtz));
}

my $parser = DateTime::Format::Strptime->new(
    pattern => $opt->get_date_format,
    time_zone => $tz,
) or die 'failed to create parser';

my $division_id_map = $dbh->selectall_hashref('select id, division from division', 'division');
my $defaults = $dbh->selectrow_hashref('select * from import_default limit 1');

my $today = DateTime->now(time_zone => $tz);
#$today->add(days => -30);

my ($over_adjusted, $inserted, $skipped, $failed, $adjusted) = (0, 0, 0, 0, 0);

$dbh->begin_work;

my $data = GOS::Spreadsheet::read_file($opt->get_file, \@fields);

system("mysqldump -K --routines -u $ENV{DB_USER} -p$ENV{DB_PASS} -h$ENV{DB_HOST} gos_" . $league . " event event_package event_local_vod > " . $league . '-'.time().'.sql');

create_teams($data);

clear_events();
validate_duration($data);
create_events($data);

$dbh->commit;

my $summary = "\n" . <<SUMMARY;
Total inserted: $inserted
Total skipped: $skipped
Total failed: $failed
Total adjusted: $adjusted
Total over adjusted 30 minutes: $over_adjusted
SUMMARY

INFO $summary;

sub clear_events {
    $dbh->do('DELETE FROM event WHERE start > ' . $today->epoch);
}

sub validate_duration {
    my $data = shift;

    foreach my $r (@$data) {
        $r->{start} = parse_date($r->{date}) or
            die 'Invalid date ' . $r->{date};
    }

    my @sorted = sort {
        $a->{arena} cmp $b->{arena} or DateTime->compare($a->{start}, $b->{start});
    } @$data;

    my $prev;
    my $prev_end;
    my $prev_loc = '';
    my @future_events;
    my $location_id_map = $dbh->selectall_hashref('select id, location from location', 'location');

    foreach my $r (@sorted) {
        my $start = $r->{start};

        if (!$start) {
            INFO 'Invalid date value ' . $r->{date};
            ++$skipped;
            next;
        }

        # Ignore past events
        if ($start < $today) {
            #DEBUG 'Skipped old dated: ' . $r->{date};
            ++$skipped;
            next;
        }

        my $loc = $location_id_map->{$r->{arena}};

        if (!$loc) {
            INFO 'location ' . $r->{arena} . ' not found, ' . $r->{date};
            ++$skipped;
            next;
        }

        if ($r->{arena} ne $prev_loc) {
            $prev = undef;
            $prev_loc = $r->{arena};
        }

        # if overlaps. 
        if ($prev && $start->epoch - $prev->{end}->epoch < 120) {
            my $diff = $prev->{end} - $start;
            my ($h, $m) = $diff->in_units('hours', 'minutes');

            $prev->{end} = $start->clone;
            $prev->{end}->add(minutes => -2);

            ++$adjusted;

            DEBUG "Adjusting prev event end time. diff: $h:$m";
            DEBUG 'prev: '. $prev->{end}->strftime('%D-%T') . ', curr: ' . $start->strftime('%D-%T');
            if ($h * 60 + $m > 30) {
                ++$over_adjusted;
            }
        }

        $r->{end} = $start->clone;

        my $minutes = $r->{age_group} && $cfg->{duration}->{ $r->{age_group} };

        $minutes ||= $opt->get_duration;

        $r->{end}->add(minutes => $minutes);
        $prev = $r;

        push @future_events, $r;
    }
    DEBUG scalar @future_events . ' events to insert';
    @$data = @future_events;
}

sub create_events {
    my $data = shift;
    my $team_id_map;
    my $rs = $dbh->selectall_arrayref('select t.id, t.short_name team_code, d.division from team t
        inner join division d on d.id = t.division_id');

    foreach my $r (@$rs) {
        $team_id_map->{$r->[2]}->{$r->[1]} = $r->[0];
    }

    my $packages = $dbh->selectcol_arrayref(<<SQL);
    SELECT id FROM subscription_plan WHERE default_package = 1
SQL

    my $tmpl = $dbh->selectall_hashref(<<SQL, 'tmpl_name');
    SELECT id, tmpl_name, location_id, sport, cam_record, cam_live, primary_live 
    FROM view_template
SQL

    foreach my $t (keys %$tmpl) {
        my %cam;
        $cam{$_}->{record} = 1 foreach (split ',', delete $tmpl->{$t}->{cam_record});
        $cam{$_}->{broadcast} = 1 foreach (split ',', delete $tmpl->{$t}->{cam_live});
        $cam{$tmpl->{$t}->{primary_live}}->{primary_live} = 1 
            if $tmpl->{$t}->{primary_live};

        $tmpl->{$t}->{camera} = \%cam;
    }

    foreach my $row( @$data ) {
        my $view_tmpl = $tmpl->{$row->{arena}};

        unless ($view_tmpl) {
           INFO 'skipped missing view tmpl name ' . $row->{arena} . ' at ' . $row->{date};
           ++$skipped;
           next;
        }

        my $age_group = $row->{age_group};
        my $category = $row->{category};
        my $division = $row->{division} || $row->{age_group} . ' ' . $category;
        my $start = $row->{start};
        my $end = $row->{end};

        my $team_id1 = $team_id_map->{$division}->{$row->{team1}} ||
            $team_id_map->{$division}->{UNK};

        unless ($team_id1) {
            $team_id1 = 
            ERROR "team not found $row->{team1} under $division at $row->{date}";
            ++$skipped;
            next;
        }

        my $team_id2 = $team_id_map->{$division}->{$row->{team2}} ||
            $team_id_map->{$division}->{UNK};

        unless ($team_id2) {
            ERROR "team not found $row->{team2} under $division at $row->{date}";
            ++$skipped;
            next;
        }

        $row->{playoff} //= '';
        my %new = (
            %$defaults,
            start => $start->epoch,
            end => $end->epoch,
            broadcast => 1,
            rebroadcast => 1,
            uplaod => 1,
            team_id1 => $team_id1,
            team_id2 => $team_id2,
            location_id => $view_tmpl->{location_id},
            division_id => $division_id_map->{$division}->{id},
            playoff => $row->{playoff} eq 'True',
            source => 'add',
            local_record => 1,
        );            

        _create_event(\%new, $packages, $view_tmpl) && ++$inserted || ++$failed;
    }
}

sub _create_event {
    my ($row, $packages, $view_tmpl) = (@_);
    state @fields;
    @fields = event_fields() unless @fields;
    state $sth_insert_event = sth_insert_event(@fields);

    my @data;

    foreach my $f (@fields) {
        my $val = exists $row->{$f} ? $row->{$f} : undef;
        $val = undef if defined $val && $val eq '';

        push @data, $val;
    }

    my $id = try {
        $sth_insert_event->execute(@data);

        my $id =  $dbh->last_insert_id(undef, undef, 'evnet', 'id')
            or die "failed to insert event";

        #- Assign Package
        foreach my $pid (@$packages) {
            $dbh->do(<<SQL, undef, $pid, $id);
            INSERT INTO event_package(id_package, id_event) values(?,?)
SQL
        }

        #- Assign Camera
        while (my ($camera, $setting) = each %{ $view_tmpl->{camera} }) {
            my $setting = $view_tmpl->{camera}->{$camera};
            my @bind = ($id, $camera, $setting->{broadcast}, $setting->{primary_live},
                $setting->{record}, 'local');

            $dbh->do(<<SQL, undef, @bind);
            INSERT INTO event_stream
            (event_id, camera, broadcast, primary_live, record, record_at) values
            (?,?,?,?,?,?)
SQL
        }
        return $id;
    } catch {
        ERROR $_;
        undef;
    };
    return $id;
}

sub get_teams {
    return $dbh->selectall_arrayref('select d.id division_id, t.id team_id, d.division,
        t.short_name as team_code from division d left join team t on d.id = t.division_id',
        { Slice => {} }
    );
}

sub create_teams_simple {
    my $data = shift;
    my $rs = get_teams();
    my $current;

    foreach my $row (@$rs) {
        $current->{$row->{division}}->{$row->{team_code}} = $row->{team_id};
    }

    my $map;
    foreach my $row (@$rs) {
        $map->{$row->{division}}->{$row->{team1}} = 1;
        $map->{$row->{division}}->{$row->{team2}} = 1;
    }
    #TODO create new teams/divisions 
}

sub create_teams {
    my $data = shift;
    my %age_group_to_code = reverse %{$cfg->{age_group}};

    my %current;
    my $st_insert_division = $dbh->prepare(<<SQL);
        INSERT INTO division(division, active, visible) values(?, ?, ?)
SQL

    my $st_insert_team = $dbh->prepare(<<SQL);
        INSERT INTO team(division_id, name, active, league, short_name, consent)
        values (?,?,?,?,?,?)
SQL

    my $rs = get_teams();

    foreach my $row (@$rs) {
        my ($age_group, $cat) = $row->{division} =~ /^(.+)\s(A{1,})$/ or next;

        next unless $age_group =~ /[^\s]+/;

        $current{$age_group}->{$cat} //= {};

        my $team_code = $row->{team_code} || next;
        $current{$age_group}->{$cat}->{$team_code} = $row->{team_id};
    }

    my %map;

    foreach my $row (@$data) {
        $map{$row->{age_group}}->{$row->{category}}->{$row->{team1}} = 1;
        $map{$row->{age_group}}->{$row->{category}}->{$row->{team2}} = 1;
    }

    foreach my $age_group( keys %map ) {
        foreach my $category ( keys %{ $map{$age_group} } ) {
            my $division = $age_group . ' ' . $category;

            unless ($current{$age_group}->{$category}) {
                # create division
                try {
                    $st_insert_division->execute($division, 1, 1);
                } catch {
                    ERROR $_;
                    undef;
                } or next;

                my $id = $dbh->last_insert_id(undef, undef, 'division', 'id');

                if (!$id) {
                    ERROR "failed to get last insert id for division $division";
                    next;
                }

                $division_id_map->{$division} = {
                    id => $id,
                    division => $division,
                };

                $current{$age_group}->{$category} = {};
            }

            foreach my $team (keys %{ $map{$age_group}->{$category} }) {
                unless ($current{$age_group}->{$category}->{$team}) {
                    # create team
                    unless ($division_id_map->{$division}) {
                        ERROR "division id not found for $age_group $category  - $team";
                        next;
                    }

                    unless ($cfg->{team_code}->{$team}) {
                        unless ($current{$age_group}->{$category}->{'UNK'}) {
                            $dbh->do('INSERT INTO team(division_id, name, active, league, short_name
                                ) values (?,?,?,?,?)', undef,
                                $division_id_map->{$division}->{id}, 'Unknown', 1, $league,
                                'UNK');

                            $current{$age_group}->{$category}->{'UNK'} = $dbh->last_insert_id(
                                undef, undef, 'team', 'id'
                            );
                        }
                        next;
                    }

                    DEBUG "inserting $team";

                    try {
                        $st_insert_team->execute(
                            $division_id_map->{$division}->{id},
                            $cfg->{team_code}->{$team},
                            1,
                            uc($league), 
                            $team,
                            2,
                        ) || die "failed to create team";

                        $current{$age_group}->{$category}->{$team} =
                            $dbh->last_insert_id(undef, undef, 'team', 'id');

                    } catch {
                        ERROR $_;
                        undef;
                    }; 
                }
            }
        }
    }
}

sub parse_date {
    my ($str) = @_;

    my $date = $parser->parse_datetime($str);
    return $date;
}

sub event_fields {
    my @fields = (qw/start end broadcast rebroadcast upload team_id1 team_id2 location_id
        division_id overlay_visible playoff dir source local_record inmediate max_periods sport visibility 
        event_visible vdir last_period_duration manual_schedule scope/);
    
    my $sth = $dbh->column_info(undef, undef, 'event', '%');
    my @col;

    while (my $r = $sth->fetchrow_hashref) {
        push @col, $r->{COLUMN_NAME} if any { $_ eq $r->{COLUMN_NAME} } @fields;
    }

    return @col;
}

sub sth_insert_event {
    my @fields = @_;
    my $f = join ', ', @fields;
    my $p = join ', ', map { '?' } @fields;

    return $dbh->prepare(sprintf(<<SQL, $f, $p));
    INSERT INTO event(%s) values(%s)
SQL
}

sub log_level {
    return $cfg->{l4p}->{level};
}

sub log_filename {
    return $cfg->{l4p}->{filename};
}

sub usage {
    my $f = __FILE__;
    say <<USAGE;
    $f -f <filepath> -d <event duration in minutes> 
USAGE

}
