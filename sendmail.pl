use Email::MIME;
use Email::Sender::Simple qw/sendmail/;
use Email::Sender::Transport::SMTP::TLS qw//;

my $input;
{
    local $/;
    $input = <STDIN>;
}

my $msg = Email::MIME->create(
    attributes => {
        content_type => 'text/plain',
        encoding => 'quoted-printable',
        charset => 'US-ASCII',
    },
    header_str => [
        From => 'info@gameonstream.com',
        To => 'dhaval070@gmail.com',
        Subject => $ARGV[0] // "from sendmail",
    ],
    body_str => $input,
);

my $smtp_config = {
    host => 'send.smtp.com',
    port => 2525,
    username => 'register',
    password => 'T1sShoodb3better999!',
    helo => 'gameonstream.com',
};

sendmail($msg, {
    transport => Email::Sender::Transport::SMTP::TLS->new($smtp_config),
});

