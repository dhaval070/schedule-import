#!/usr/bin/perl
#This script converts rchl schedule file to import by schedule-import.pl
use strict;
use warnings;
use 5.010;
use Data::Dumper qw/Dumper/;
use lib 'lib';
use FindBin qw/$Bin/;
use GOS::Config qw/$cfg load_config/;
use Log::Log4perl qw/:easy/;
use Getopt::Lucid qw/:all/;
use GOS::DB qw//;
use GOS::Spreadsheet qw//;
use DateTime;
use DateTime::Format::Strptime;
use Try::Tiny qw/try catch/;
use List::Util qw/any first/;
use Text::CSV qw();

usage() unless @ARGV;

load_config($Bin . '/conf/config.pl');
load_config("$Bin/conf/team_codes.conf", 'team_code');

my $c = <<L4P;
log4perl.logger = sub { log_level() . ', main' }
log4perl.appender.main = Log::Log4perl::Appender::File
log4perl.appender.main.filename = sub { log_filename() }
log4perl.appender.main.mode=append
log4perl.appender.main.layout=PatternLayout
log4perl.appender.main.layout.ConversionPattern=[%d] %p %F:%L - %m%n
L4P

Log::Log4perl->init(\$c);

my @spec = (
    Param('file|f'),
    Param('league|l'),
);

my $opt = Getopt::Lucid->getopt(\@spec)->validate({requires => [qw/file league /]});
my $dbh = GOS::DB->connect($opt->get_league, $cfg->{db_user}, $cfg->{db_password});

my @fields = qw/date time team1 team2 venue division/;

my $rs = $dbh->selectall_arrayref('select name, short_name, d.division from team join division d on team.division_id=d.id', { Slice => {} });
my $teams;
foreach my $row (@$rs) {
    $teams->{$row->{division}}->{$row->{name}} = $row->{short_name};
}

my $vt = $dbh->selectcol_arrayref('select tmpl_name from view_template');
my $divisions = $dbh->selectcol_arrayref('select division from division');

# Tue., Oct. 10 7:30 PM
my $parser = DateTime::Format::Strptime->new(
    #pattern => '%a., %b. %d %Y %I:%M %p',
    pattern => '%m/%d/%y %H:%M',
    time_zone => 'local',
) or die 'failed to create parser';

my $data = GOS::Spreadsheet::read_file($opt->get_file, \@fields);

my @out;
my %team_missing;
my %missing_vanue;
my %missing_division;
my $next_year = DateTime->now->strftime('%Y') + 1;    
my $current_year = $next_year - 1;

my $i = 0;

my %months = map { $_ => ++$i } (qw/Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec/);
my $current_month = $months{ DateTime->now->strftime('%b') };

foreach my $row (@$data) {
    my $time = $row->{time};

    if ($time =~ /^\d:/) {
        $time = '0' . $time;
    }

    #$row->{date} =~ s/^([a-zA-Z]{3}., [a-zA-Z]{3}. )([0-9])$/${1}0$2/;
    # Fri., Dec. 22
    
    #my ($month) = $row->{date} =~ /^[a-zA-Z]+., ([a-zA-Z]+)/;
    #my $year = ($current_month - $months{$month} > 6) ? $next_year : $current_year; 

    #my $date = $row->{date} . " $year $time";
    $row->{date} = sprintf('%02d/%02d/%02d', split('/', $row->{date}));

    my $date = $row->{date} . " $time";
    say Dumper $date;
    my $dt = $parser->parse_datetime($date) || die 'failed to parse date ' . $date;

    say $row->{division} . ' ' . $row->{team1} . ' ' . $row->{team2};

    my $division = find_division($row->{division}) or next;
    #my $team1 = find_team($row->{team1}, $division) or next;
    #my $team2 = find_team($row->{team2}, $division) or next;

    my $tmpl = find_vanue($row->{venue}) or next;

    push @out, [ $dt->strftime('%m/%d/%y %H:%M'), $row->{team1}, $row->{team2}, $division, $tmpl ];
}

say 'outrows ' . scalar @out;

my $csv = Text::CSV->new({ binary => 1, eol => "\n"}) or die Text::CSV->error_diag;
open my $fh, '>:encoding(utf8)', 'rchl.csv' or die $!;

$csv->print($fh, [ 'Date', 'Home Team', 'Away Team', 'Division', 'Venue' ]);

foreach my $row (@out) {
    say join(', ', @$row);
    $csv->print($fh, $row);
}

close $fh;

sub find_division {
    my $name = shift;

    return if $missing_division{$name};

    if (any { $_ eq $name } @$divisions) {
        return $name;
    }
    $missing_division{$name} = 1;
    ERROR "division $name not found";
    return;
}

sub find_team {
    my $name = shift;
    my $div = shift;

    return if $team_missing{$div}->{$name};

    if ($teams->{$div}->{$name}) {
        return $teams->{$div}->{$name};
    }

    $team_missing{$div}->{$name} = 1;
    ERROR 'Team ' . "$name not matched under division $div";
    return;
}

sub find_vanue {
    my $name = shift;
    my $div = shift;

    return if $missing_vanue{$name};

    my $tmpl = first { $_ eq $name } @$vt;
    unless ($tmpl) {
        ERROR 'vanue ' . $name . ' not found';
        $missing_vanue{$name} = 1;
        return;
    }
    return $name;
}

sub log_level {
    return $cfg->{l4p}->{level};
}

sub log_filename {
    return $cfg->{l4p}->{filename};
}

sub usage {
    my $f = __FILE__;
    say <<USAGE;
    $f -l <league> -f <filepath>
USAGE

}
